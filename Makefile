.PHONY : flask init

run : 
	@echo "1st step run make init"
	@echo "2nd step run source venv/bin/activate"
	@echo "3rd step  make flask"
	
init :	
	@virtualenv -p python3 venv
	@echo "now you need to the 2nd step and then the 3rd "

flask:
	@pip install flask
	@pip install flask-sqlalchemy
	@pip install flask-wtf
	@pip install python-dotenv
	@pip install pyyaml
	@flask loaddb Appli/data.yml
	@echo "congratulation now you can run flask with this virtual environment"
