from flask import Flask
app = Flask(__name__)
#######################################
import os.path
def mkpath(p):
	return os.path.normpath(os.path.join(os.path.dirname(__file__),p))
#######################################
from flask_sqlalchemy import SQLAlchemy
app.config['SQLALCHEMY_DATABASE_URI']=('sqlite:///'+mkpath('../myapp.db'))
db = SQLAlchemy(app)

app.config['SECRET_KEY'] = "7daf395a-ec5a-4f35-a98e-1b0d7e500c0a"