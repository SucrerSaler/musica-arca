import click
from .app import app, db
#import des modèles
from .models import Album, Artist, Genre, get_artist, get_genre

@app.cli.command()
def syncdb():
    '''
     Création de toutes les tables de la BD
    '''
    db.create_all()

@app.cli.command()
@click.argument('filename')
def loaddb(filename):
	'''Creates the tables and populates them with data'''
	#creation des tables
	db.drop_all()
	db.create_all()

	#chargement de notre jeux de données
	import yaml
	albums = yaml.load(open(filename),Loader=yaml.SafeLoader)

	#premiere passe: creation de tout les genres
	genre_dict={}
	for a in albums:
		genres_album = a["genre"]
		for g in genres_album:
			if g not in genre_dict:
				genre = Genre(name = g)
				db.session.add(genre)
				genre_dict[g] = genre
	db.session.commit()

	#deuxieme passe: creation de tous les artistes
	artists={}
	for a in albums:
		artist = a["by"]
		genre = a["genre"]
		photo_artist = "No.jpg" if "photo" not in a else a["photo"]
		if artist not in artists:
			o = Artist(name = artist,
					   photo = photo_artist)
			db.session.add(o)
			artists[artist] = o
					
	db.session.commit()

	#troisieme passe: ajout des genres aux artistes
	for a in albums:
		artist = a["by"]
		genre = a["genre"]
		artist_id = artists[artist].id
		db_artist = get_artist(artist_id)
		for g in genre:
			if get_genre(genre_dict[g].id) not in db_artist.genres:
				db_artist.genres.append(genre_dict[g])
		
	db.session.commit()

	#quatrieme passe: creation de tous les albums	
	for a in albums:
		artist = artists[a["by"]]
		sId = "" if "spotifyId" not in a else a["spotifyId"]
		dId = "" if "deezerId" not in a else a["deezerId"]
		image = "No.jpg" if a["img"] == "" else a["img"]
		album = Album(title = a["title"],
				  releaseYear = a["releaseYear"],
				  img = image,
				  spotifyId = sId,
				  deezerId = dId,
				  artist_id = artist.id)
		for g in a["genre"]:
			album.genres.append(genre_dict[g])
		db.session.add(album)
	db.session.commit()
