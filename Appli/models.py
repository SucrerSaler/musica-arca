from .app import db

asso_artist_genre = db.Table("asso_artist_genre", db.Model.metadata,
                             db.Column("artist_id", db.ForeignKey("artist.id"), primary_key=True),
                             db.Column("genre_id", db.ForeignKey("genre.id"), primary_key=True)
                             )

asso_album_genre = db.Table("asso_album_genre", db.Model.metadata,
                             db.Column("album_id", db.ForeignKey("album.id"), primary_key=True),
                             db.Column("genre_id", db.ForeignKey("genre.id"), primary_key=True)
                             )

class Album(db.Model):
    __tablename__ = "album"
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50))
    releaseYear = db.Column(db.Integer)
    img = db.Column(db.String(100))
    spotifyId = db.Column(db.String(100))
    deezerId = db.Column(db.String(100))
    artist_id = db.Column(db.Integer,
                          db.ForeignKey("artist.id"))
    artist = db.relationship("Artist",
                             backref=db.backref("albums", lazy="dynamic"))
    genres = db.relationship("Genre", secondary=asso_album_genre)

    def __repr__(self):
    	return "<Album (%d) %s>" % (self.id, self.name)

class Artist(db.Model):
    __tablename__ = "artist"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    photo = db.Column(db.String(100))
    genres = db.relationship("Genre", secondary=asso_artist_genre)

    def __repr__(self):
    	return "<Artist (%d) %s>" % (self.id, self.name)

class Genre(db.Model):
    __tablename__ = "genre"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True)

    def __repr__(self):
    	return "<Genre (%d) %s>" % (self.id, self.name)

# ----------------------[request]----------------------

def get_top_album(nb):
    return Album.query.limit(nb).all()

def get_album(entryId):
    return Album.query.get_or_404(entryId)

def get_albums():
    return Album.query.all()

def get_top_artist(nb):
    return Artist.query.limit(nb).all()

def get_artists():
    return Artist.query.all()

def get_artist(entryId):
    return Artist.query.get_or_404(entryId)

def get_albums_of_artist(entryId):
    return get_artist(entryId).albums.all()

def get_albums_by_date(date):
    return Album.query.filter(Album.releaseYear==date)

def get_albums_by_date_asc():
    return Album.query.order_by(Album.releaseYear.asc())

def get_albums_by_date_desc():
    return Album.query.order_by(Album.releaseYear.desc())

def get_genres():
    return Genre.query.all()

def get_genre(entryId):
    return Genre.query.get_or_404(entryId)

def get_albums_of_genre(genreId):
    return Album.query.filter(Album.genres.contains(get_genre(genreId))).all()

def get_artists_of_genre(genreId):
    return Artist.query.filter(Artist.genres.contains(get_genre(genreId))).all()

def get_Top_albums_of_genre(genreId,nb):
   return Album.query.filter(Album.genres.contains(get_genre(genreId))).limit(nb).all()

def get_Top_artists_of_genre(genreId,nb):
   return Artist.query.filter(Artist.genres.contains(get_genre(genreId))).limit(nb).all()

def add_an_artist(name_a):
    artist = Artist(name=name_a)
    db.session.add(artist)
    db.session.commit()

def add_a_album(title_a, release):
    album = Album(title=title_a, releaseYear=release)
    db.session.add(album)
    db.session.commit()

def add_a_genre(name_g):
    genre = Genre(name=name_g)
    db.session.add(genre)
    db.session.commit()

def delete_a_artist(artist):
    db.session.delete(artist)
    db.session.commit()

def delete_a_album(album):
    db.session.delete(album)
    db.session.commit()

def delete_a_genre(genre):
    db.session.delete(genre)
    db.session.commit()
