from .app import app
from flask import render_template, request, redirect, url_for
from .models import *
from flask_wtf import FlaskForm
from wtforms import StringField , HiddenField, IntegerField
from wtforms. validators import DataRequired

@app.route('/', methods=['GET'])
def home():
	return render_template('home.html',albums=get_top_album(4),artists=get_top_artist(4))

@app.route('/connexion', methods=['GET'])
def connexion():
	return render_template('connexion.html')

@app.route('/inscription', methods=['GET'])
def inscription():
	return render_template('inscription.html')

@app.route('/albumId', methods=['GET'])
def albumId():
	return render_template('albumId.html')

@app.route('/artisteId', methods=['GET'])
def artisteId():
	return render_template('artisteId.html')

@app.route('/album/<int:id>', methods=['GET'])
def albumById(id):
	return render_template('albumId.html', album=get_album(id))

@app.route('/artist/<int:id>', methods=['GET', 'POST'])
def artistById(id):
	'''
	a = get_artist(id)
	if request.method == 'POST':
		add_artist(a.name)
	'''
	return render_template('artisteId.html', artist=get_artist(id),albumArtist=get_albums_of_artist(id))

@app.route('/admin', methods=['GET'])
def admin():
	return render_template('admin.html', artist=get_album(id))

@app.route('/favoris', methods=['GET'])
def favori():
	return render_template('favori.html', artist=get_album(id))

@app.route('/ajouter', methods=['GET', 'POST'])
def ajouter():
	return render_template('ajouter.html', artist=get_album(id))

@app.route('/supprimer', methods=['GET', 'POST'])
def supprimer():
	return render_template('supprimer.html', artist=get_album(id))

@app.route('/modifier', methods=['GET', 'POST'])
def modifier():
	return render_template('modifier.html', artist=get_album(id))

@app.route('/albums', methods=['GET'])
def pageAlbums():
	page = request.args.get('page', 1, type=int)
	data = Album.query.paginate(page=page, per_page=12, error_out=True)
	return render_template("albums.html",data=data,topAlbums=get_top_album(4))

@app.route('/artists', methods=['GET'])
def pageArtists():
	page = request.args.get('page', 1, type=int)
	data = Artist.query.paginate(page=page, per_page=12, error_out=True)
	return render_template("artistes.html",data=data,topArtists=get_top_artist(4))

@app.route('/genres', methods=['GET'])
def pageGenres():
	page = request.args.get('page', 1, type=int)
	data = Genre.query.paginate(page=page, per_page=24, error_out=True)
	return render_template("genres.html",data=data)	

@app.route('/genre/<int:id>', methods=['GET'])
def pageGenresId(id):
	return render_template("genreId.html", artists=get_Top_artists_of_genre(id,4),albums=get_Top_albums_of_genre(id,4),idGenre=get_genre(id))

@app.route('/albums/genre/<int:id>', methods=['GET'])
def pageAlbumsGenres(id):
	page = request.args.get('page', 1, type=int)
	data = Album.query.filter(Album.genres.contains(get_genre(id))).paginate(page=page, per_page=12, error_out=True)
	return render_template("albumGenreId.html",data=data,idGenre=get_genre(id))

@app.route('/artists/genre/<int:id>', methods=['GET'])
def pageArtistsGenres(id):
	page = request.args.get('page', 1, type=int)
	data = Artist.query.filter(Artist.genres.contains(get_genre(id))).paginate(page=page, per_page=12, error_out=True)
	return render_template("artistGenreId.html",data=data,idGenre=get_genre(id))

class ArtistForm(FlaskForm):
    id = HiddenField('id')
    name = StringField('New name', validators=[DataRequired()])

class AlbumForm(FlaskForm):
    id = HiddenField('id')
    title = StringField('New title', validators=[DataRequired()])
    releaseYear = IntegerField('New release year', validators=[DataRequired()])
    spotifyId = StringField('New spotifyId')
    deezerId = StringField('New deezerId')

class GenreForm(FlaskForm):
    id = HiddenField('id')
    name = StringField('New name', validators=[DataRequired()])

@app.route("/edit/artist/<int:id>", methods=['GET', 'POST'])
def edit_artist(id):
    a = get_artist(id)
    f = ArtistForm(id=a.id, name=a.name)
    return render_template(
        "edit_artist.html", 
        artist=a, form=f
    )

@app.route("/edit/album/<int:id>", methods=['GET', 'POST'])
def edit_album(id):
    a = get_album(id)
    f = AlbumForm(id=a.id, title=a.title, releaseYear=a.releaseYear, spotifyId=a.spotifyId, deezerId=a.deezerId)
    return render_template(
        "edit_album.html", 
        album=a, form=f
    )

@app.route("/edit/genre/<int:id>", methods=['GET', 'POST'])
def edit_genre(id):
    a = get_genre(id)
    f = GenreForm(id=a.id, name=a.name)
    return render_template(
        "edit_genre.html", 
        genre=a, form=f
    )

class DeleteForm(FlaskForm):
    id = HiddenField('id')


@app.route("/delete/artist/<int:id>", methods=['GET', 'POST'])
def delete_artist(id):
    a = get_artist(id)
    f = DeleteForm(id=a.id)
    return render_template(
        "delete_artist.html", 
        artist=a, form=f
    )

@app.route("/delete/album/<int:id>", methods=['GET', 'POST'])
def delete_album(id):
    a = get_album(id)
    f = DeleteForm(id=a.id)
    return render_template(
        "delete_album.html", 
        album=a, form=f
    )

@app.route("/delete/genre/<int:id>", methods=['GET', 'POST'])
def delete_genre(id):
    a = get_genre(id)
    f = DeleteForm(id=a.id)
    return render_template(
        "delete_genre.html", 
        genre=a, form=f
    )

@app.route("/add", methods=['GET'])
def add():
	return render_template("add.html")


class AddArtistForm(FlaskForm):
    name = StringField('Name of the artist', validators=[DataRequired()])

class AddAlbumForm(FlaskForm):
    title = StringField('Name of the artist', validators=[DataRequired()])
    releaseYear = IntegerField('New release year', validators=[DataRequired()])

class AddGenreForm(FlaskForm):
    name = StringField('Name of the genre', validators=[DataRequired()])

@app.route("/add/artist/", methods=['GET', 'POST'])
def add_artist():
	f = AddArtistForm(name="")
	return render_template("addArtist.html", form=f)


@app.route("/add/album/", methods=['GET', 'POST'])
def add_album():
    f = AddAlbumForm(name="")
    return render_template("addAlbum.html", form=f)

@app.route("/add/genre/", methods=['GET', 'POST'])
def add_genre():
    f = AddGenreForm(name="")
    return render_template("addGenre.html", form=f)

@app.route("/deleted", methods=['GET', 'POST'])
def deleted():
    return render_template(
        "deleted.html"
    )

@app.route("/deleted/artist/", methods=["POST"])
def deleting_artist():
    a = None
    f = DeleteForm()
    if f.validate_on_submit():
        id = int(f.id.data)
        a = get_artist(id)
        delete_a_artist(a)
        db.session.commit()
        return redirect( url_for('deleted') )
    return redirect( url_for('pageArtists') )


@app.route("/deleted/album/", methods=["POST"])
def deleting_album():
    a = None
    f = DeleteForm()
    if f.validate_on_submit():
        id = int(f.id.data)
        a = get_album(id)
        delete_a_album(a)
        db.session.commit()
        return redirect( url_for('deleted') )
    return redirect( url_for('pageAlbums') )

@app.route("/deleted/genre/", methods=["POST"])
def deleting_genre():
    a = None
    f = DeleteForm()
    if f.validate_on_submit():
        id = int(f.id.data)
        a = get_genre(id)
        genre = Genre(name=a.name)
        delete_a_genre(a)
        db.session.commit()
        return redirect( url_for('deleted') )
    return redirect( url_for('pageGenres') )


@app.route("/savedArtist/<int:id>")
def one_artist(id):
    a = get_artist(id)
    return render_template("save_artist.html", artist=a)

@app.route("/savedAlbum/<int:id>")
def one_album(id):
    a = get_album(id)
    return render_template("save_album.html", album=a)

@app.route("/savedGenre/<int:id>")
def one_genre(id):
    a = get_genre(id)
    return render_template("save_genre.html", genre=a)


@app.route("/save/artist/", methods=["POST"])
def save_artist():
    a = None
    f = ArtistForm()
    if f.validate_on_submit():
        id = int(f.id.data)
        a = get_artist(id)
        a.name = f.name.data
        db.session.commit()
        return redirect( url_for('one_artist', id=a.id) )
    a = get_artist(int(f.id.data))
    return redirect( url_for('artistById', id=a.id) )

@app.route("/save/album/", methods=["POST"])
def save_album():
    a = None
    f = AlbumForm()
    if f.validate_on_submit():
        id = int(f.id.data)
        a = get_album(id)
        a.title = f.title.data
        a.releaseYear = int(f.releaseYear.data)
        a.spotifyId = f.spotifyId.data
        a.deezerId = f.deezerId.data
        db.session.commit()
        return redirect( url_for('one_album', id=a.id) )
    a = get_album(int(f.id.data))
    return redirect( url_for('albumById', id=a.id) )

@app.route("/save/genre/", methods=["POST"])
def save_genre():
    a = None
    f = GenreForm()
    if f.validate_on_submit():
        id = int(f.id.data)
        a = get_genre(id)
        a.name = f.name.data
        try:
            db.session.commit()
        except:
            return render_template("genreName_notValid.html")
        return redirect( url_for('one_genre', id=a.id) )
    a = get_artist(int(f.id.data))
    return redirect( url_for('pageGenresId', id=a.id) )


@app.route("/added")
def added(id):
    a = get_artist(id)
    return render_template("added.html", artist=a)

@app.route("/added/artist/", methods=["POST"])
def adding_artist():
    f = AddArtistForm()
    if f.validate_on_submit():
        add_an_artist(f.name.data)
        return redirect( url_for('pageArtists') )
    return redirect( url_for('add') )

@app.route("/added/album/", methods=["POST"])
def adding_album():
    f = AddAlbumForm()
    if f.validate_on_submit():
        add_a_album(f.title.data, int(f.releaseYear.data))
        return redirect( url_for('pageAlbums') )
    return redirect( url_for('add') )

@app.route("/added/genre/", methods=["POST"])
def adding_genre():
    f = AddGenreForm()
    if f.validate_on_submit():
        add_a_genre(f.name.data)
        return redirect( url_for('pageGenres') )
    return redirect( url_for('add') )